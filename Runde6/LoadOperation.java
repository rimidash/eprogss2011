import java.util.Scanner;

public class LoadOperation implements Operation{
	private AsciiImage img; 
	private final String data; // String mit Kompletten Bild und Zeilenumbruch
	
	public LoadOperation(String data){ //  Konstruktor
		this.data = data;	
	}
	public AsciiImage execute(AsciiImage img) throws OperationException{ 
		this.img = img;
		Scanner sc = new Scanner(data);
		int x = 0;
		int y = 0;
		while(sc.hasNextLine()){
			String reader = sc.nextLine();
			
			for(y = 0; y <reader.length(); y++){
				if(img.getCharset().indexOf(reader.charAt(y)) == -1){ // Falls das Zeichen nicht im Charset ist wird eine Exception geworfen
						throw new OperationException();
				}
				else if(reader.length()>img.getWidth() || reader.length()<img.getWidth()){
					throw new OperationException(); // Falls Zeilenanzahl oder Zeilenlaenge nicht korrekt sind wird eine Exception geworfen
				}
				
				img.setPixel(x,y,reader.charAt(y));
			}
			x++; // Zeile vorruecken
		}
		if(x == img.getHeight()){
			if(y == img.getWidth()){
				return img;
			}
			else{
				throw new OperationException(); // Falls Zeilenlaenge nicht korrekt sind wird eine Exception geworfen
			}
		}
		else{
		throw new OperationException(); // Falls Zeilenanzahl nicht korrekt ist wird eine Exception geworfen
		}
	}
}
