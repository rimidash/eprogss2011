public class LineOperation{

	public boolean stringHelper (String[] data){ // Diese Methode wertet mehrere Kommandos aus. Kontrolliert wird lediglich ob diese Korrekt zusammengesetzt sind.
		String help = "";
		for(int i = 0 ; i < data.length ; i++){
			help+= data[i];
		}
		
		if(help.matches("create[0-9]{1,3}[0-9]{1,3}[.[^0-9]]+")){ // Create gefolgt von einem 1 oder 2 Stelligem Zahlenwert gefolgt von einem 1 oder 2 Stelligem Zahlenwert gefolgt von einem oder mehreren beliebigen Zeichen das keine Zahl ist
			return true;
			}
		else if(help.matches("load(.)+")){ // load gefolgt von einem oder mehreren Zeichen
			return true;
			}
		else if(help.matches("filtermedian")){ // nur falls nach dem Wort Filter auch Median kommt.
			return true;
			}
			else if(help.matches("replace(.){2}")){ // replace gefolgt von exakt 2 beliebigen Zeichen. 
			return true;
			}
	
		
		return false;
	}

}
