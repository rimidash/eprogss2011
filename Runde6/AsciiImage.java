import java.util.ArrayList;

public class AsciiImage{
	private final String charset; // Liste moeglicher Zeichen im Bild
	private final int height; 
	private final int width;
	private char[][] image; // Bild als 2D Array
	
	public AsciiImage(int width, int height, String charset){ // Konstruktor
		
		this.height = height;
		this.width = width;		
		this.charset = charset;
		image = new char[height][width];
		ClearOperation op = new ClearOperation(); // Aufruf der Clear Operation
		op.execute(this);
	}
	
	public AsciiImage(AsciiImage img){  // Kopierkonstruktor Tiefe Kopie
		charset = img.charset;
		height = img.height;
		width = img.width;
		image = new char[height][width];
		for (int x = 0; x < img.getHeight(); x ++){
			for(int y = 0; y < img.getWidth(); y++){
				setPixel(x,y,img.getPixel(x,y));
			}
		}
	}
	
	public void setPixel(int x, int y, char c){ // SetPixel Methode
		if(charset.indexOf(c) > -1){ // wird nur ausgefuehrt falls das Zeichen im Charset vorkommt.
		image[x][y] = c;
		}
	}
	public void setPixel(AsciiPoint p, char c){ // SetPixel Methode
		if(charset.indexOf(c) > -1){ // wird nur ausgefuehrt falls das Zeichen im Charset vorkommt.
			image[p.getX()][p.getY()]=c;
		}
	}
	public String getCharset(){
		return charset;
	}
	public char getPixel(int x, int y){
		return image[x][y];
	}
	public char getPixel(AsciiPoint p){
		return image[p.getX()][p.getY()];
	}
	public int getHeight(){
		return height;
	}
	public int getWidth(){
		return width;
	}
	
	public ArrayList<AsciiPoint> getPointList(char c){ // Arraylist die mehrere AsciiPoints speichert
		ArrayList<AsciiPoint> pointlist = new ArrayList<AsciiPoint>();
		for (int x = 0; x < getHeight(); x ++){
			for(int y = 0; y < getWidth(); y++){
				if(getPixel(x,y) == c){
					pointlist.add(new AsciiPoint(x,y));
				}
			}
		}
		return pointlist;
	}
	
	public String toString(){
        String out = "";
        
        for(int i=0; i<getHeight(); i++){
            for(int j=0; j<getWidth(); j++){
                //out += "" + image[j][i];
                //out += Arrays.deepToString(image[i][j]);
                out += image[i][j];
            }
            
            out = out + "\n";//Zeilenumbruch am Ende der Zeile einfuegen
        }
        /*
        for(int i=0; i<image.length; i++){
            out += Arrays.toString(image[i])+"\n";
            //out = out + "\n";//Zeilenumbruch am Ende der Zeile einfuegen
        }*/
        
        
        //test toString
        System.out.print(out + "ZU");
        
        
        //test toString
        //System.out.println(out + "toString");
        
        return out;
    }  
/*	public String toString(){
		String output = "";
		
		for (int x = 0; x < getHeight(); x ++){
			for(int y = 0; y < getWidth(); y++){
				output += getPixel(x,y);
			}
			output += "\n";
		}
		return output;
	}
	
	*/
}
