import java.util.Arrays;

public class MedianOperation implements Operation{
	private String charset; // Liste der Verfuegbaren Zeichen
	private AsciiImage img;
	
	public MedianOperation() {	
	}
	
	public AsciiImage execute(AsciiImage img){
		charset = img.getCharset(); // Charset holen
		this.img = new AsciiImage(img); // Kopie erstellen
		
		// Diese Schleifen durchlaufen das Komplette Bild und rufen fuer jeden Pixel die Methode MedianChar auf.
		for (int xr = 0; xr < img.getHeight(); xr ++){ 
			for(int yr = 0; yr < img.getWidth(); yr++){
				img.setPixel(xr,yr,MedianChar(xr,yr));
			}
		}
		return img;
		
	}
	
	// Diese Methode erstellt ein Array aus einem Pixel und seinen 8 Nachbarpixeln ( wobei Pixel ausserhalb des Bildes die hellste Farbe bekommen
	// Dannach wird das Array nach helligkeitswerten sortiert und der Median ( Mittelpunkt ) als neuer Pixel gesetzt.
	private char MedianChar(int a, int b){
		int[] hell = new int[9];
		int i = 0;
			for(int x = (a-1); x< (a+2);x++){
				for(int y =(b-1); y<( b+2); y++){
					
					if(x < 0){
						hell[i]=charset.length()-1;
					}
					else if(x >= img.getHeight()){
						hell[i]=charset.length()-1;
					}
					else if(y < 0){
						hell[i]=charset.length()-1;
					}
					else if(y >= img.getWidth()){
						hell[i]=charset.length()-1;
					}
					else{
						hell[i]=charset.indexOf(img.getPixel(x,y));
					}
					i++;
					
				}
			}
		Arrays.sort(hell);
		
		return charset.charAt(hell[4]);
	}
}	
