import java.util.*;

public class AsciiShop{
	
	public static void main(String[] args){
		
		Scanner scan = new Scanner(System.in); 
		String[] tokens = scan.nextLine().split(" "); // Tokenizer fuer Eingaben
		Operation op; // Operarion Variable
		Stack <AsciiImage>stack = new Stack<AsciiImage>(); // Implementierung des Stacks
		LineOperation lo = new LineOperation(); // Hilfsklasse fuer die Eingabeueberpruefung
		if(tokens[0].equals("create")){ // Create nur zu beginn
			
			
			if(lo.stringHelper(tokens)){ // Aufruf der Hilfsklasse
				AsciiImage bild = new AsciiImage(Integer.parseInt(tokens[1]),Integer.parseInt(tokens[2]),tokens[3]);
				int y = Integer.parseInt(tokens[2]);
				while(scan.hasNextLine()){ // komplette Eingabe ansehen
					tokens = scan.nextLine().split(" ");
					
					
					try { 
						if(tokens[0].equals("load")){ 
							int x = 0; // Counter fuer Zeilenanzahl
							stack.push(new AsciiImage(bild)); // Kopie auf den Stack legen
							String reader = ""; // Hilffsstring für Uebergabe
							if(lo.stringHelper(tokens)){ // Aufruf der Hilfsklasse zur Ueberpruefung
								while(scan.hasNextLine()){
									
									String read  = scan.nextLine();
								
									
									if(read.equals(tokens[1])){ // Eof Bedingung
										break;
									}
									else if(x == y && !read.equals(tokens[1])){ // Zuviele oder zuwenig Zeilen
										System.out.println("INPUT MISMATCH");
										break;
									}
								
								
								else{
										reader += read+"\n";
										x++;
									}
									
								}
								op = new LoadOperation(reader); // Uebergabe
								bild = op.execute(bild);
								
							}
						}
						else if(tokens[0].equals("filter")){ // Aufruf Median Filter
							if(lo.stringHelper(tokens)){ // Hilfsklasse ueberprueft Eingabe 
								stack.push(new AsciiImage(bild));
								op = new MedianOperation();
								op.execute(bild);
							}
							else {
								System.out.println("INPUT MISMATCH");
								break;
							}
						}
						else if(tokens[0].equals("clear")){ // Loeschbefehl
							stack.push(new AsciiImage(bild));
							op = new ClearOperation();
							op.execute(bild);
						}
						else if(tokens[0].equals("replace")){ // Replace Befehl
							if(lo.stringHelper(tokens) == false){ // Hilfsklasse ueberprueft Eingabe
								System.out.println("INPUT MISMATCH");
								break;
							}
							stack.push(new AsciiImage(bild));
							op = new ReplaceOperation(tokens[1].charAt(0),tokens[2].charAt(0));
							bild =op.execute(bild);
						}
						else if(tokens[0].equals("print")){ // Bildschirmausgabe
							System.out.println(bild);
						}
						else if(tokens[0].equals("undo")){ // UNDO Befehl
							if(stack.isEmpty()){ // falls der Stack bereits leer ist
								System.out.println("STACK EMPTY");
							}
							else{ // Bild vom Stack nehmen und zuweisen
								bild = stack.pop();
							}
						}
						else {  // bei allen anderen Kommandos
							System.out.println("UNKNOWN COMMAND");
						}
						
						
						
					}
					catch(OperationException ex){ // fangen der Exception und Ausbruch aus Schleife
						System.out.println("OPERATION FAILED");
						break;
					}
					
				}
				
			}
			
			else {
				System.out.println("INPUT MISMATCH");
			}
			
		}
		
		else {
			System.out.println("INPUT MISMATCH");
		}
		
	}	
	
	
}
