public class ClearOperation implements Operation{
	
	public ClearOperation(){
	}
	
	public AsciiImage execute(AsciiImage img){
		char c = img.getCharset().charAt(img.getCharset().length()-1); // Hellsten Char waehlen
		for (int x = 0; x < img.getHeight(); x ++){
			for(int y = 0; y < img.getWidth(); y++){
				img.setPixel(x,y,c); // Komplettes Bild mit hellstem Char ueberschreiben
			}
		}
		return img;
	}
		
}
