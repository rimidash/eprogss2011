import java.util.Scanner;

public class AsciiShop {
	private static Scanner scan = new Scanner(System.in); // Scanner erzeugen
	
	public static void main(String[] args) {
		String[] tokens = scan.nextLine().split(" "); // Array für Kommandos und deren Eingabewerte
		AsciiImage img = null; // neues AsciiImage erzeugen
		boolean commands = true; // Richtige Kommandos eingegenen ?
		boolean mismatch = true; // Input Mismatch boolean
		AsciiStack stack = new AsciiStack(); // neuen AsciiStack erzeugen
		
		if(tokens[0].equals("create")){
			
			img = new AsciiImage(Integer.parseInt(tokens[1]),Integer.parseInt(tokens[2])); // Elemente Aus tokens auf Int casten und dem neuen AsciiImage uebergeben
			
			
			while(scan.hasNextLine()){
				tokens = scan.nextLine().split(" "); // bei jedem Leerzeichen Splitten und ins Array einfuegen
				
				if(tokens[0].equals("load")){
					AsciiImage oldbild = new AsciiImage(img); // Bild dem Kopierkonstruktor uebergeben 
					stack.push(oldbild); // Bild auf den Stack pushen
					int x= 0, y = 0; // Integer zur Ueberpruefung der Laenge und Hoehe
					while(scan.hasNextLine()){
						
						String read = scan.nextLine(); // naechste Zeile im String speichern
						
						y = read.length();
						if(tokens[1].equals(read)){  // Abbruchbedingung End of Load
							break;
						}
						else if (y == img.getLength() && x <= img.getHeight()){ //AsciiImage befuellen
							for(int i= 0; i < y; i ++){
								img.setPixel(x,i,read.charAt(i));
							}
							x++;
						}
						else{
						mismatch = false;} // bei Fehler in Laenge oder Hoehe wird INPUT MISMATCH ausgegeben
						
					}
				}
				else if(tokens[0].equals("print")){ //Print Kommando
					System.out.println(img.toString());
				}	
				else if(tokens[0].equals("clear")){ //Clear Kommando und Stack Push
					AsciiImage oldbild = new AsciiImage(img);
					stack.push(oldbild);
					img.clear();
				}
				else if(tokens[0].equals("transpose")){ //Transpose Kommando
					img.transpose();
				}
				else if(tokens[0].equals("line")){ //line Kommando und Stack Push
					AsciiImage oldbild = new AsciiImage(img);
					stack.push(oldbild);
					img.drawLine(Integer.parseInt(tokens[1]),Integer.parseInt(tokens[2]),Integer.parseInt(tokens[3]),Integer.parseInt(tokens[4]),tokens[5].charAt(0));
				}
				else if(tokens[0].equals("replace")){ //replace Kommano und Stack Push
					AsciiImage oldbild = new AsciiImage(img);
					stack.push(oldbild);
					img.replace(tokens[1].charAt(0),tokens[2].charAt(0)); 
				}
				else if(tokens[0].equals("centroid")){ // Centroid Kommando
					System.out.println(img.getCentroid(tokens[1].charAt(0)));		
				}
				else if(tokens[0].equals("grow")){ //grow Kommando und Stack Push
					AsciiImage oldbild = new AsciiImage(img);
					stack.push(oldbild);
					img.growRegion(tokens[1].charAt(0)); 
				}
				else if(tokens[0].equals("undo")){ //undo Kommando 
					AsciiImage newBild = stack.pop();
					if(newBild == null){
						System.out.println("STACK EMPTY");	// Ausgabe bei leerem Stack
					}
					else {
						img = newBild;
						System.out.println("STACK USAGE " + stack.size() + "/"+stack.capacity()); // Ausgabe bei nicht leerem Stack
					}
					
				}
				else if(tokens[0].isEmpty() == false){ // Falsches Kommando eingegeben.
					commands = false;			
				}		
			} 
			if(commands == false){
				System.out.println("UNKNOWN COMMAND");
			}	
			
			else if(mismatch == false) {
				System.out.println("INPUT MISMATCH");
			}
		}
		
		
		
		
		
	}
}
