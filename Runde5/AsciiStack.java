public class AsciiStack{
	private AsciiImage[] img= new AsciiImage[3]; //Array aus AsciiImages
	private int i = 0; // Anzahl der Elemente am Stack
	private final static int increment = 3;
	public void push(AsciiImage bild){ //Neues Bild auf den Stack legen
		if(i < 0){ //Falls trotz leerem Stack mehrere undo ausgefuehrt werden
			
		i = 0;
		img[i] = bild;
		i++;
		}
		else if(i < img.length){ // Stack nicht voll
			img[i] = bild;
			i++;
		}
		else if(i == img.length){ // Falls der Stack voll ist neues Array mit der groesse vom alten + Increment erzeugen und die Bilder vom alten in das Neue Array kopieren
			
			AsciiImage[] img2 = new AsciiImage[i+increment];
			for (int c = 0; c<img.length; c ++){
				
				System.arraycopy(img,0,img2,0,img.length);
			}
			img=img2;
			img[i]=bild;
			i++;
		}
	
	}
	public AsciiImage pop(){ // Bild vom Stapel nehmen
		i--;
		
		if(empty()){
		
		
		if((img.length -i)>increment){ // neues kleineres Array anlegen und kopien vom alten Array uebernehmen
			
			AsciiImage[] img2 = new AsciiImage[img.length-increment];
			
				
				System.arraycopy(img,0,img2,0,img2.length);
			
			img=img2;
			
			return img[i];
		}
		return img[i];
		}
		else{
			return null;
		}
	
	}
	public int capacity(){ // Ausgabe der Kapazitat des Stacks
		return img.length;
	}
	public boolean empty(){ // Abfrage fuer leerem Stack
		boolean empty=true;
		if(i < 0){
			empty =false;
			}
			return empty;
	}
	public int size(){ // Ausgabe der groesse
		return i;
	}
	private void resize(){  // Stack resizen
		AsciiImage newstack[] = new AsciiImage[i];
		
		System.arraycopy(img,0,newstack,0,Math.min(img.length,newstack.length));
		img = newstack;
	}
	public AsciiImage peek(){ // oberstes Bild am Stack ausgeben
		if(empty()){
			return null;
		}
		return img[i-1];
	}
	
}
