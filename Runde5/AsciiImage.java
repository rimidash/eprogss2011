import java.util.ArrayList;

public class AsciiImage {
	
	private char[][] bild; //mehrdimensionales Char Array
	private static int h,l; // hoehe und laenge
	
	public AsciiImage(int x, int y){ //Konstruktor 
		this.h = x;
		this.l = y;
		bild = new char[y][x];
		clear();
		
	}
	
	public AsciiImage(AsciiImage img){ //Kopierkonstruktor
		h = img.h;
		l = img.l;
		bild = new char[l][h];
		for (int i = 0; i<l; i ++){
			
			System.arraycopy(img.bild[i],0,bild[i],0,h);
		}
	}
	
	public void clear(){ // Bild mit . fuellen
		for(int i = 0; i < bild.length; i ++){
			for(int c = 0; c <bild[0].length; c++){
				setPixel(i,c,'.');
			}
		}
	}
	
	ArrayList<AsciiPoint> getPointList(char c){ // alle AsciiPoints mit einem bestimmten Char ausgeben
		ArrayList<AsciiPoint> list = new ArrayList<AsciiPoint>();
		
		for(int i = 0; i < getHeight(); i ++){
			for(int e = 0; e <getLength(); e++){
				if(getPixel(e,i) == c){
					
					list.add(new AsciiPoint(i,e));
				}
			}
		}
		
		return list;
	}
	
	public String toString(){  // Ausgabe
		String bildausgabe ="";
		
		for(char[]e : bild){
			for(char f: e){
			bildausgabe += Character.toString(f);
			}
			bildausgabe += "\n";
		}
		return bildausgabe;
	}
	
	public void setPixel(int x, int y, char c){ // Setter fuer einzellne Bildpunkte
		bild[x][y]= c;
	}
	
	private void setPixel2(int x, int y, char c){ // Setter fuer Bildpunkte der Methode growRegion
		if(bild[x][y] == '.'){
			bild[x][y]=c;
		}
	}
	
	public char getPixel(int x, int y){ // Getter fuer Pixel
		return bild[y][x];
	}
	
	public char getPixel(AsciiPoint p){ // Getter fuer Pixel als AsciiPoint
		return bild[p.getY()][p.getX()];
	}
	
	
	public int getLength(){ // Getter fuer die laenge des Bildes
		return bild[0].length;	
	}
	public int getHeight(){ // Getter fuer die hoehe des Bildes
		return bild.length;
	}
	
	public void transpose(){
		char[][] bild1= new char[h][l]; //Neues mehrdimensionales Array erzeugen dessen Hoehe und Laenge vertauscht sind.
		for (int e = 0; e<h; e++) { 
			for(int a = 0; a<l; a++) {
				
				//Bildpunkte umschreiben
				bild1[e][a] = bild[a][e];
			}
		}
		//Bild Ueberschreiben
		bild = bild1; 
		//Hoehe und Breite umschreiben
		int a = l;
		l = h;
		h = a;
	}
	public void drawLine(int x0, int y0, int x1, int y1, char c){
		double dx = x1-x0; // Berechnung Delta X
		double dy = y1 - y0; // Berechnung Delta Y
		double ansteigend; // ansteigend auf X oder Y Achse
		double delta; // Dx durch Dy
		
		// Fall 1
		if(Math.abs(dy)<=Math.abs(dx) && dx>=0){ // Aufsteigend dan der X Achse
			delta=dy/dx;
			ansteigend = y0;
			for (int a=x0;a<=x1;a++){
				setPixel(y0,a,c); // SetPixel aufrufen und den entsprechenden Pixel zeichnen
				ansteigend+=delta; // Ansteigend erhoehen
				y0=(int)Math.round(ansteigend); // Ansteigend Runden und auf y0 casten.
			}
			
		}
		
		// Fall 2		
		else if(Math.abs(dy)<=Math.abs(dx) && dx<0){ 
			ansteigend = y0;
			delta = dy/dx;
			for (int a=x0;a>=x1;a--){
				setPixel(y0,a,c);
				ansteigend-=delta;
				y0 = (int)Math.round(ansteigend);
			}
			
		}
		
		// Fall 3
		else if (Math.abs(dy)>Math.abs(dx) && dy>=0){
			ansteigend = x0;
			delta = dx/dy;
			for ( int a =y0; a<=y1;a++){
				setPixel(a,x0,c);
				ansteigend +=delta;
				x0 =(int)Math.round(ansteigend);
			}
		}
		
		// Fall 4
		else if (Math.abs(dy)>Math.abs(dx) && dy<0){
			ansteigend = x0;
			delta = dx/dy;
			for ( int a =y0; a>=y1;a--){
				setPixel(a,x0,c);
				ansteigend -=delta;
				x0 =(int)Math.round(ansteigend);
			}
			
		}
		
		
	}
	public void replace(char oldChar, char newChar){ // Alle Vorkommnisse eines alten Char mit neuem Char ersetzen
		for(int i = 0; i < bild.length; i ++){
			for(int c = 0; c <bild[0].length; c++){
				if(bild[i][c] == oldChar){
					setPixel(i,c,newChar);
				}
			}
		}
	}
	
	public AsciiPoint getCentroid(char c){ //Schwerpunkt bestimmen
		ArrayList<AsciiPoint> list = getPointList(c);
		if(list.size() == 0){
			return null;
		}
		
		else{
			double x= 0, y = 0;
			for(int i = 0; i<list.size(); i ++){
				x += list.get(i).getX();
				y += list.get(i).getY();
				
			}
			
			x = x/list.size();
			y = y/list.size();
			
			AsciiPoint p = new AsciiPoint((int)Math.round(y),(int)Math.round(x));
			return p;
		}
		
	}
	
	public void growRegion(char c){ //Benachbarte Punkte wachsen lassen
		for(int i = 0; i < getHeight(); i ++){
			for(int e = 0; e <getLength(); e++){
				if(getPixel(e,i) == c){
					if( e == 0 && i == 0){ //1
						
						setPixel2(i+1,e,' ');
						setPixel2(i,e+1,' ');
					}
					else if(i == getHeight()-1 && e == getLength()-1){//eck3
						setPixel2(i,e-1,' ');
						setPixel2(i-1,e,' ');
					}
					else if(i == getHeight()-1 && e == 0){//eck4
						setPixel2(i-1,e,' ');
						setPixel2(i,e+1,' ');
						
					}
					else if(e == getLength()-1 && i == 0){//eck2
						setPixel(i,e-1,' ');
						
						setPixel(i+1,e,' ');
					}
					else if(e == 0 && i < getHeight()-1){//kante 4
						setPixel2(i-1,e,' ');
						setPixel2(i+1,e,' ');
						setPixel2(i,e+1,' ');
					}
					else if(i == 0 && e < getLength()-1){//kante 1
						setPixel2(i,e+1,' ');
						setPixel2(i,e-1,' ');
						setPixel2(i+1,e,' ');
					}
					else if(e == getLength()-1 && i < getHeight()-1){//kante 2
						setPixel2(i,e-1,' ');
						setPixel2(i+1,e,' ');
						setPixel2(i,e-1,' ');
						
					} 
					else if(i == getHeight()-1 && e < getLength()-1){//kante 3
						setPixel2(i-1,e,' ');
						setPixel2(i,e-1,' ');
						setPixel2(i,e+1,' ');
						
					}
					
					else{ //nicht am Rand
						setPixel2(i,e-1,' ');
						setPixel2(i-1,e,' ');
						setPixel2(i,e+1,' ');
						setPixel2(i+1,e,' ');
						
					}
					
				}
			}
		}
		replace(' ',c);
	}
	
}

