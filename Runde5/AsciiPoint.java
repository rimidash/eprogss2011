public class AsciiPoint{
	private final int x,y; // Koordinaten eines AsciiPoints
	
	public AsciiPoint(int x, int y){ //Konstruktor zum zuweisen
	this.x = x;
	this.y = y;
	
	}
	public int getX(){ // Getter
		return x;
	}
	public int getY(){ // Getter
		return y;
	}
	public String toString(){ // Ausgabe
		String punkt = "("+getX()+","+getY()+")";
		return punkt;
	}

}
