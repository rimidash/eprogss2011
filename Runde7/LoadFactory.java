import java.util.Scanner;


public class LoadFactory implements Factory {
	
	public Operation create(Scanner scan) throws FactoryException {
		
		String[] arguments = scan.nextLine().trim().split(" ");//Tokenizer
		// Falls zuviel oder zuwenig Argumente eingegeben wurden wird eine Exception geworfen.
		if(arguments.length !=1){
			throw new FactoryException("INPUT MISMATCH");
		}
		
	String eof = arguments[0].trim(); // EoF Bestimmtn
	
	String data = ""; // String mit Kompletten Bilddaten
	
	while (scan.hasNextLine()){
		
		String line = scan.nextLine().trim();	
		
		// Stoppen falls die EoF Bedingung erreicht wurde.
		if( line.equals(eof)){
			break;
		}
		 
	data+= line + "\n";
	
	
	
	}
	
	
	return new LoadOperation(data);
	}
	

}
