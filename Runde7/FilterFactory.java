import java.util.Scanner;


public class FilterFactory implements Factory {

	public Operation create(Scanner scan) throws FactoryException {
		
		String[] arguments = scan.nextLine().trim().split(" "); // Tokenizer
		
		// Falls auf Filter nicht median folgt wird eine Exception geworfen.
		if(!arguments[0].equals("median")){
			throw new FactoryException("INPUT MISMATCH");
		}
		return new MedianOperation();
	}

	

}
