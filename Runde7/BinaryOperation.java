public class BinaryOperation implements Operation{
	
	private char threshold , dark , bright; // Threshold , dunkelster und hellster Char 
        private String charset; // aktuelles Charset
        private int index; // Index von Threshold
        
        public BinaryOperation(char threshold) {
        	this.threshold = threshold;
        }
        
        
        public AsciiImage execute(AsciiImage img) throws OperationException{
        	
        	charset = img.getCharset();
        	dark = charset.charAt(0);
        	bright = charset.charAt(charset.length()-1);
        	index = charset.indexOf(threshold);
        	
        	// Falls der Threshold Char nicht im Charset vorkommt
        	if(index < 0){
        		throw new OperationException();
        	}
        	
        	AsciiImage output = new AsciiImage(img);
        	
        	// Bild durchlaufen und je nach Charposition auf Hell oder Dunkel setzen
        	for (int x = 0; x < img.getHeight(); x ++){
			for(int y = 0; y < img.getWidth(); y++){
				output.setPixel(x,y,switchChar(output.getPixel(x,y)));
			}
		}
        	
        	return output;
        	
        	
        }
        
        // Hilfsmethode um zu bestimmen ob der aktuelle Character Hell oder Dunkel wird.
        
        private char switchChar(char c){
        	if(charset.indexOf(c) < index){
        		return dark;
        		
        	}
        	else return bright;
        	
        	
        }
        
        
        
        
} 
