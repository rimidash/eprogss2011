import java.util.Scanner;

public class BinaryFactory implements Factory {

	
	public Operation create(Scanner scan)throws FactoryException {
		
		String[] arguments = scan.nextLine().trim().split(" "); // Tokenizer
		
		// Falls zuviel oder zu wenig Argumente 
		if(arguments.length != 1){ 
			throw new FactoryException("INPUT MISMATCH");
		}
		
		return new BinaryOperation(arguments[0].charAt(0));
	}

}
