import java.util.*;

public abstract class FilterOperation implements Operation{
	protected String charset;
	protected AsciiImage img;
	
	
	
	public FilterOperation(){

		
	}
	
	public AsciiImage execute(AsciiImage img){
		
		charset = img.getCharset();// Charset holen
                this.img = new AsciiImage(img); // Kopie erstellen
                
                // Diese Schleifen durchlaufen das Komplette Bild und 
                // rufen fuer jeden Pixel die Methode MedianChar auf.
                for (int xr = 0; xr < img.getHeight(); xr ++){
                        for(int yr = 0; yr < img.getWidth(); yr++){
				int[] values = new int[] {xr,yr};
                                img.setPixel(xr,yr,filter(values));
                        }
                }
                return img;
		
	}
	
	public abstract char filter(int[] values);
}
