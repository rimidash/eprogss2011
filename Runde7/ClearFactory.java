import java.util.Scanner;

public class ClearFactory implements Factory {

	public Operation create(Scanner scan) throws FactoryException {

		return new ClearOperation();

	}

}
