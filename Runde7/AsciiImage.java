import java.util.ArrayList;


public class AsciiImage {
	private char[][] bild;
	private int height;
	private int width;
	private String charset;

	public AsciiImage(int y, int x, String charset)throws OperationException{
		this.charset = charset;
		height = x;
		width = y;
		bild = new char[x][y];
		Operation op = new ClearOperation(); // Verwendung der ClearOperation um doppelten Code zu vermeiden
		op.execute(this);

	}
	
	// Kopierkonstruktor
	public AsciiImage(AsciiImage img){ 
		charset = img.charset;
		height = img.height;
		width = img.width;
		bild = new char[height][width];
		for (int x = 0; x < img.getHeight(); x ++){
			for(int y = 0; y < img.getWidth(); y++){
				setPixel(x,y,img.getPixel(x,y));
			}
		}

	}
	public char getPixel(int x, int y) {

		return bild[x][y];
	}
	public char getPixel(AsciiPoint p){ 
		return bild[p.getX()][p.getY()];
	}
	
	public void setPixel(int x, int y, char c) {
		bild[x][y]= c;

	}
	public void setPixel(AsciiPoint p, char c){ 
                if(charset.indexOf(c) > -1){
                        bild[p.getX()][p.getY()]=c;
                }
        }
	public int getWidth() {

		return width;
	}
	public int getHeight() {

		return height;
	}
	public String getCharset() {

		return charset;
	}
	// Erstellt eine Liste mit sämtlichen Koordinaten eines Zeichens
	public ArrayList<AsciiPoint> getPointList(char c){
		ArrayList<AsciiPoint> pointlist = new ArrayList<AsciiPoint>();
		for (int x = 0; x < getHeight(); x ++){
			for(int y = 0; y < getWidth(); y++){
				if(getPixel(x,y) == c){
					pointlist.add(new AsciiPoint(x,y));
				}
			}
		}
		return pointlist;
	}
	
	public String toString(){ 
		String bildchen = "";
		
		for (int x = 0; x < getHeight(); x ++){
			for(int y = 0; y < getWidth(); y++){
				bildchen += getPixel(x,y);
			}
			bildchen += "\n";
		}
		
		return bildchen;
	}
}
