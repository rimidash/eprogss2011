import java.util.*;

public class AsciiShop {
	private static HashMap<String,Factory> Command = new HashMap<String,Factory>(); // Map für Kommandos
	private static Scanner scan = new Scanner(System.in);
	private static AsciiImage img;
	private static Stack<AsciiImage> stack = new Stack<AsciiImage>(); // Undo Stack

	public static void main(String[] args){
		initCmd(); // Aufruf um Kommandos in die Map zu speichern
		
		String[]token = scan.nextLine().trim().split(" "); // Tokenizer
		try{
			if(token[0].equals("create")){
				
				img = new AsciiImage(Integer.parseInt(token[1]),Integer.parseInt(token[2]),token[3]);
				while(scan.hasNext()){
					handleCmd(scan); // Aufruf der Hilfsklasse zum Command Handling

				}
			}
			else throw new Exception("INPUT MISMATCH"); // Exception bei fehlendem create
		}
		catch(Exception e){

			System.out.println(e.getMessage()); // Exceptions fangen und deren Message Ausgeben
		}

	}
	
	// Hilfsklasse die die alle Kommandos ausser Create abwickelt. //
	// Durch das Interface Factory werden die passenden Methoden   //
	// dynamisch gewählt. Lediglich bei Print und undo ist dies    //
	// nicht möglich da sich diese nicht in dem Interface befinden //
	private static void handleCmd(Scanner scan) 
	throws FactoryException, OperationException { // kann beide Exceptionarten werfen
		Operation op; 
		AsciiImage oldImg = new AsciiImage(img); // Kopie anlegen für den Stack
		String cmd = scan.next(); // Kommando einlesen
		
		// Falls das Kommando in der Map vorkommt. Wird es der entsprechenden Factory uebergeben
		// Dannach erzeugt die Factory die dazu passende Operation welche ebenfalls ausgefuehrt wird.
		if(Command.containsKey(cmd)){ 
			op = Command.get(cmd).create(scan);
			img = op.execute(img);
			stack.push(oldImg);
		// Sonderfall Print da diese Methode in AsciiImage eingebettet ist.	
		}else if(cmd.equals("print")){
			System.out.println(img.toString());
			scan.nextLine();
		
		}
		// Sonderfall UNDO da dieser in AsciiShop eingebettet ist.
		else if(cmd.equals("undo")){
			if(stack.isEmpty()){
				System.out.println("STACK EMPTY");
			}
			else{
				img = stack.pop();
			}
			scan.nextLine();
		}
		//Sonderfall Histogramm da dies nicht in den Interfaces vorkommt.
		else if(scan.equals("histogramm")){


		}
		// bei unbekannten befehlen wird abgebrochen
		else throw new FactoryException("UNKNOWN COMMAND");


	}
	
	// Einlesen der möglichen Kommandos
	private static void initCmd(){

		Command.put("load",new LoadFactory());
		Command.put("clear",new ClearFactory());
		Command.put("binary",new BinaryFactory());
		Command.put("filter",new FilterFactory());
		Command.put("replace",new ReplaceFactory());
	}
}

