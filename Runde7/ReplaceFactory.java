import java.util.Scanner;


public class ReplaceFactory implements Factory {

	public Operation create(Scanner scan) throws FactoryException {
		
		String arguments[] = scan.nextLine().trim().split(" ");// Tokenizer
		
		// Bei zuvielen oder zuwenigen Argumenten wird eine Exception geworfen.
		if(arguments.length != 2){
			throw new FactoryException("INPUT MISMATCH");
		}
		
		return new ReplaceOperation(arguments[0].charAt(0),arguments[1].charAt(0));
	}

}
