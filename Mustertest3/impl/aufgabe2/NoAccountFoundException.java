public class NoAccountFoundException extends Exception{

	/**
	 * Default Konstruktor.
	 */
	public NoAccountFoundException() {
		super();
	}
	
	/**
	 * Konstruktor mit einer Nachricht.
	 * 
	 * @param message Fehlerbeschreibung als String
	 */
	public NoAccountFoundException(String message) {
		super(message);
	}

}
