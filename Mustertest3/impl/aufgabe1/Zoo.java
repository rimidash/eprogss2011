/*
 * 1. Aufgabe: Typbeziehungen
 * 
 * Der Inhalt dieser Datei wird nicht bewertet.
 */
public class Zoo {
	
	public static void main(String[] args) {
		
		Animal a = new Tiger();
		System.out.println(a.eatsMeat() + " " + a.dailyFeedQuantity());
		
		a = new Loewe();
		System.out.println(a.eatsMeat() + " " + a.dailyFeedQuantity());
		
		a = new Elefant();
		System.out.println(a.eatsMeat() + " " + a.dailyFeedQuantity());
		
		a = new Schaf();
		System.out.println(a.eatsMeat() + " " + a.dailyFeedQuantity());
		
	}
	
}