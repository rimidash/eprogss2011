/*
 * 3. Aufgabe: Rekursion und Collections
 * 
 * Der Inhalt dieser Datei wird nicht bewertet.
 */
public class Tester {
	
	public static void main(String[] args) {
	
		/*
		 * Fall 1
		 * "red" ist die am häufigsten vorkommende Farbe
		 */
		{
			B test = new B();
			System.out.println(test);
		}
		
	}
	
}