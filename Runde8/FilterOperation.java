
public abstract class FilterOperation implements Operation {
	protected String charset;
	protected AsciiImage image;
	protected AsciiImage img;
	
	
	// Superklasse von Average und Medianfilter, Das Bild wird durchlaufen und
	// dynamisch je nach Kommando entweder Average oder Medianfilter aufgerufen.
	// sowie das Pixel entsprechend gesetzt.
	public AsciiImage execute(AsciiImage img) throws OperationException {
		
		image = new AsciiImage(img);
		this.img = img;
		charset = img.getCharset();
		for (int x = 0; x < img.getHeight(); x++){
			for(int y = 0; y < img.getWidth(); y++){
				image.setPixel(x,y,filter(new int[] {x,y}));
			}
		}
		return image;
	}

	public abstract char filter(int[] is);

}
