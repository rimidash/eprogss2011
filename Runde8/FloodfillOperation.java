
public class FloodfillOperation implements Operation {
	private AsciiImage image;
	private String charset;
	private int a,b;
	private final char c;
	private char fromChar;

	// Konstruktor 
	public FloodfillOperation(int y, int x, char c) {
		a = x;
		b = y;
		this.c = c;
	}
	
	// Floodfill Operation fuellt ausgehend von einem Startpixel alle umliegenden
	// gleichen Pixel mit einem spezifizierten Char.
	
	public AsciiImage execute(AsciiImage img) throws OperationException {
		image = new AsciiImage(img);

		charset = image.getCharset();
		
		// Falls der zum fuellen verwendete Char oder der angegebene Startpixel ausserhalb
		// des Bildes ist wird eine Exception geworfen und abgebrochen.
		if(charset.indexOf(c) < 0 || !img.InBounds(a,b)){
			throw new OperationException("OPERATION FAILED");
		}
		
		fromChar = image.getPixel(a, b);
		// recursiveFill fuellt das Bild wie bereits oben beschrieben.
		recursiveFill(a,b);

		return image;
	}
	
	// Rekursive Methode die ausgehend von Anfangskoordinaten alle gleichen Pixel
	// äendert bis keine gleichen Pixel mehr vorhanden sind.
	// Dafuer gibt es in der Klasse AsciiImage die Methode getNeighbours welche ein
	// 3 x 3 Pixel großes Feld um den jeweiligen Startpixel spezifiziert und auf gleichheit
	// ueberprueft und gegebenenfalls ändert.
	private void recursiveFill(int x, int y) {
		if(image.getPixel(x, y) != fromChar) return;

		image.setPixel(x, y, c);
		
		for (AsciiPoint p : image.getNeighbours(x, y)){
			recursiveFill(p.getX(),p.getY());
		}
	}

}
