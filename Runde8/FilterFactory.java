import java.util.Scanner;


public class FilterFactory implements Factory {

	public Operation create(Scanner scan) throws FactoryException {
		String[] read = scan.nextLine().trim().split(" "); // Tokenizer
		
		// Bei zu langem oder zu kurzem Kommando wird eine Exception geworfen
		if(read.length != 1){
			throw new FactoryException("INPUT MISMATCH");
		}
		else if (read[0].equals("median")){
			return new MedianOperation();
		}
		else if (read[0].equals("average")){
			return new AverageOperation();
		}
		
		// Falls weder Median noch average nach Filter Folgen wird eine Exception geworfen.
		else{
			throw new FactoryException("INPUT MISMATCH");
		}
	}

	

}
