
public class BinaryOperation implements Operation {
	private char limit; // Char der den Schwellenwert repraesentiert

	public BinaryOperation(char value){
		limit = value;
	}

	
	public AsciiImage execute(AsciiImage img) throws OperationException {
		String charset = img.getCharset();
		// Falls der Schwellenwert Char nicht im charset ist schlaegt die Operation Fehl
		if(charset.indexOf(limit) == -1){
			throw new OperationException("OPERATION FAILED");
		}
		else{
			
			// Bild durchlaufen und entweder auf Hellsten oder dunkelsten Char setzen je nachdem
			// ob der aktuelle Char heller oder dunkler dem Schwellenwert ist.
			for(int x = 0; x < img.getHeight(); x++ ){
				for(int y = 0; y < img.getWidth(); y++){
					if(charset.indexOf(img.getPixel(x, y)) < charset.indexOf(limit)){
						img.setPixel(x, y, charset.charAt(0));
					}
					else{
						img.setPixel(x,y,charset.charAt(charset.length()-1));

					}
				}
			}
		}
		return img;
	}

}
