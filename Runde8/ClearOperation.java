
public class ClearOperation implements Operation {

// Bild durchlaufen und den alle Chars auf den im Charset hellsten Char setzen.
	public AsciiImage execute(AsciiImage img) throws OperationException {
		for(int x = 0; x <img.getHeight(); x++){
			for(int y = 0; y < img.getWidth(); y++){
				img.setPixel(x,y,img.getCharset().charAt(img.getCharset().length()-1));
			}
		}
		
		return img;
	}

}
