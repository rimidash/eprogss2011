import java.util.Scanner;


public class LoadFactory implements Factory {
	private String data ="";

	public Operation create(Scanner scan) throws FactoryException {
		String[] args = scan.nextLine().trim().split(" ");
		// Bei falscher Kommandolaenge wird eine Exception geworfen.
		if(args.length != 1){
			throw new FactoryException("INPUT MISMATCH");
			
		}
// Alle Zeilen werden in einem String gespeichert und zusammen mit der Klasse
// LoadOperation aufgerufen.
			String eof = args[0];
			while (scan.hasNextLine()){
				String line = scan.nextLine().trim();
				if(line.equals(eof)){
					break;
				}
				data += line +"\n";

			
		}
		return new LoadOperation(data);
	}
	

}
