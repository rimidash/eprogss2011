import java.util.Scanner;


public interface Factory {

	Operation create(Scanner scan) throws FactoryException;

}
