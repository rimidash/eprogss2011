import java.util.Scanner;


public class FloodfillFactory implements Factory {


	public Operation create(Scanner scan) throws FactoryException {
		String[] read = scan.nextLine().trim().split(" ");
		// Bei falscher Kommandolaenge wird eine Exception geworfen.
		if(read.length != 3){
			throw new FactoryException("INPUT MISMATCH");
		}
		return new FloodfillOperation(Integer.parseInt(read[0]),Integer.parseInt(read[1]),read[2].charAt(0));
	}

}
