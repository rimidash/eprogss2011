import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;


// 
public class AsciiShop {
	private static HashMap<String,Factory> command = new HashMap<String,Factory>();
	private static Scanner scan = new Scanner(System.in);
	private static AsciiImage img;
	private static Stack<AsciiImage> stack = new Stack<AsciiImage>();
	
	public static void main(String[] args){
		initializeCmd(); // Verfuegbare Kommandos einlesen
		String[]token = scan.nextLine().split(" "); // Tokenizer fuer erste Eingabe.
		
		
		try{
			// Weitere Bildbearbeitung wird nur ermoeglicht
			// wenn der erste Befehlr Create lautet ansonsten
			// bricht das Programm mit der Ausgabe UNKNOWN Command
			//   ab
			
			if(token[0].equals("create")){
				img = new AsciiImage(Integer.parseInt(token[1]),Integer.parseInt(token[2]),token[3]);
				while(scan.hasNext()){
			// Diese Methodenaufruf bearbeitet saemtliche Eingegebenen
			// Kommandos.
					handleCmd(scan);
				}

			}
			else {
				System.out.println("UNKNOWN COMMAND");
			}
		}
		
		// Beide Exceptionarten abfangen und ausgeben.
		
		catch(FactoryException x){
			System.out.println(x.getMessage());
		}
		catch(OperationException e){

			System.out.println(e.getMessage());
		}

	}
	
	// Diese Methode Verarbeitetet alle eingegeben Kommandos bis auf Create
	// Load , Clear , Binary & Filter werden dynamisch Aufgerufen.
	// Undo und Print haben einen eigenen Statischen Aufruf
	
	private static void handleCmd(Scanner scan) throws FactoryException, OperationException {
		
		Operation op; // Operationsvariable
		AsciiImage oldImg = new AsciiImage(img); // Kopier fuer den Stack anlegen
		String cmd = scan.next(); // Kommandos uebernehmen
		
		// Dynamischer Aufruf von Klassen fuer Load , Clear , Binary , Filter
		if(command.containsKey(cmd)){
			op = command.get(cmd).create(scan);
			img = op.execute(img);
			stack.push(oldImg);
		}
		
		// Statischer Aufruf fuer Print
		else if(cmd.equals("print")){
			System.out.println(img.toString());
			scan.nextLine();
		}
		
		// Statischer Aufruf fuer Undo 
		else if(cmd.equals("undo")){
			
			// um Runtime Exceptions zu vermeiden wird bei
			// leerem Stack Stack Empty ausgegeben.
			// Bei vollem Stack wird das oberste Bild
			// auf dem Stack zurueckgegeben.
			if(stack.isEmpty()){
				System.out.println("STACK EMPTY");
			}
			else{
				img = stack.pop();
			}
			scan.nextLine();

		}
		// Falls Kommandos eingegeben werden die nicht Vorhanden sind.
		else throw new FactoryException("UNKNOWN COMMAND");


	}
	
	// Methode um alle Kommandos einzulesen die moeglich sind.
	private static void initializeCmd(){

		command.put("load",new LoadFactory());
		command.put("clear",new ClearFactory());
		command.put("binary",new BinaryFactory());
		command.put("filter",new FilterFactory());
		command.put("replace",new ReplaceFactory());
		command.put("floodfill",new FloodfillFactory());
	}
}

