import java.util.Scanner;

public class LoadOperation implements Operation{
        private AsciiImage image;
        private String data;
        private String reader;
        
        public LoadOperation(String data){
                this.data = data;
                
        }
        /*
        * Ueberprueft sowohl die Zeilen als auch die Zeichen groesse
        * und speichert diese, sofern das Zeichen im Charset enthalten ist
        * in das AsciiImage. Bei fehler wirft es eine Exception.
        *
        */
        public AsciiImage execute(AsciiImage img) throws OperationException{
                image = img;
                Scanner sc = new Scanner(data);
                int x = 0; //Zeilen counter
                int y = 0; //Zeichencounter
                while(sc.hasNextLine()){
                	
                        reader = sc.nextLine();
                        for(y = 0; y <reader.length(); y++){
                        	
                        	// Bei nicht im Charset vorkommenden Character wird eine Exception geworfen.
                        	if(isValidChar(y) == false){
                        	throw new OperationException("OPERATION FAILED");
                        	}
                        		img.setPixel(x,y,reader.charAt(y));
                        	
                        	
                        }
                        x++;
                }
                
                return image;
                
        }
        
        // Hilfsmethode um herauszufinden ob der uebergebene Char im Charset enthalten ist.
        private boolean isValidChar(int y){
        	if(image.getCharset().indexOf(reader.charAt(y)) == -1){
        		return false;
        	}
        	return true;
        }
}

