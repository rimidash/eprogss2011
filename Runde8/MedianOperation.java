import java.util.Arrays;


public class MedianOperation extends FilterOperation{

	public MedianOperation() {
		
	}

	public char filter(int[] values){
		/**
		*Erstellt für ein 3x3 Feld um den uebergeben Punkt,
		*und speicher alle Zeichen in ein Array. Sollte 
		*ein Punkt des Feldes ausserhalbt des Bildes liegen
		*wird automatisch der hellste Punkt angenommen.
		*danach wird das Array sortiert, und das mittlere Zeichen
		*wird zurückgebeben.
		**/
		int[] hell = new int[9];
		int i = 0;
			for(int x = (values[0]-1); x< (values[0]+2);x++){
				for(int y =(values[1]-1); y<(values[1]+2); y++){
					
					if(x < 0){
						hell[i]=charset.length()-1;
					}
					else if(x >= image.getHeight()){
						hell[i]=charset.length()-1;
					}
					else if(y < 0){
						hell[i]=charset.length()-1;
					}
					else if(y >= image.getWidth()){
						hell[i]=charset.length()-1;
					}
					else{
						hell[i]=charset.indexOf(img.getPixel(x,y));
					}
					i++;
					
				}
			}
		Arrays.sort(hell);
		
		return charset.charAt(hell[4]);
	}
}