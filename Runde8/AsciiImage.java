import java.util.ArrayList;

public class AsciiImage {
	
        private char[][] image;
        private int height;
        private int width;
        private String charset;
        
        // Kontstruktor mit Clear Operation
        public AsciiImage(int y, int x, String charset) throws OperationException{
                this.charset = charset;
                height = x;
                width = y;
                Operation op = new ClearOperation();
                image = new char[x][y];
                op.execute(this);
                
        }
        
        // Kopierkonstruktor fuer Tiefe Kopien
        public AsciiImage(AsciiImage img){
                charset = img.charset;
                height = img.height;
                width = img.width;
                image = new char[height][width];
                for (int x = 0; x < img.getHeight(); x ++){
                        for(int y = 0; y < img.getWidth(); y++){
                                setPixel(x,y,img.getPixel(x,y));
                        }
                }
                
        }
        public char getPixel(int x, int y) {
                return image[x][y];
        }
        public char getPixel(AsciiPoint p){  
        	return image[p.getX()][p.getY()];
        }
        
        // Setzt ein Pixel auf eingegebenen Char falls
        // dieser im Charset enthalten ist.
        public void setPixel(int x, int y, char c) {
                if(charset.indexOf(c) > -1){
                        image[x][y]=c;
                }      
        }
        
       // Setzt ein Pixel auf eingegebenen Char falls
       // dieser im Charset enthalten ist.
        public void setPixel(AsciiPoint p, char c){ 
        	
                if(charset.indexOf(c) > -1){
                        image[p.getX()][p.getY()]=c;
                }
        }
        public int getWidth() {
                return width;
        }
        public int getHeight() {
                return height;
        }
        public String getCharset() {
        	
                return charset;
        }
        // Erstellt eine ArrayList mit saemtlichen Koordinaten eines Characters
        public ArrayList<AsciiPoint> getPointList(char c){
        	
                ArrayList<AsciiPoint> pointlist = new ArrayList<AsciiPoint>();
                for (int x = 0; x < getHeight(); x ++){
                        for(int y = 0; y < getWidth(); y++){
                                if(getPixel(x,y) == c){
                                        pointlist.add(new AsciiPoint(x,y));
                                }
                        }
                }
                return pointlist;
        }
        
        
        public ArrayList<AsciiPoint> getNeighbours(int x, int y){
        	ArrayList<AsciiPoint> list = new ArrayList<AsciiPoint>();
                for(int a = -1; a < 2; a++){
                        for(int b=-1; b<2; b++){
                                if(Math.abs(a+b) != 1){
                                        continue;
                                }
                                if(!InBounds(x+a,y+b)){
                                        continue;
                                }
                                list.add(new AsciiPoint(x+a, y+b));
                        }
                }
                return list;
        }
        
        // Ueberprueft ob das Zeichen in Bounds liegt
        public boolean InBounds(int i, int j) {
        	if(i < 0 || i >= height)return false;
                if(j < 0 || j >= width) return false;
                return true;
        }
        
        
        public String toString(){ 
                String output = "";
                
                for (int x = 0; x < getHeight(); x ++){
                        for(int y = 0; y < getWidth(); y++){
                                output += getPixel(x,y);
                        }
                        output += "\n";
                }
                
                return output;
        }
}
