import java.util.Scanner;


public class ReplaceFactory implements Factory {
	public ReplaceFactory(){
		
	}

	public Operation create(Scanner scan) throws FactoryException {
		String[] read = scan.nextLine().trim().split(" ");
		if(read.length != 2){
			throw new FactoryException("INPUT MISMATCH");
		}
		
		return new ReplaceOperation(read[0].charAt(0), read[1].charAt(0));
	}

}
