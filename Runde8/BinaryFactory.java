import java.util.Scanner;

public class BinaryFactory implements Factory {

	
	public Operation create(Scanner scan) throws FactoryException {
		String[] read = scan.nextLine().trim().split(" "); // Tokenizer
		if (read.length != 1){ // Exception bei falscher Eingabe des Befehls
				throw new FactoryException("INPUT MISMATCH");		
		}

		return new BinaryOperation(read[0].charAt(0));
	}
}

