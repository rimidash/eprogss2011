import java.util.*;

public class AsciiShop{
        public static void main(String [] args){
                Scanner sc = new Scanner(System.in);// Scanner für Eingaben
                String abbr; // Abbruchbedingung des load Befehls
                AsciiImage bild = new AsciiImage(); // ein neues Objekt AsciiImage erzeugen
                boolean mis = true; // boolean für Input Mismatch
                while (sc.hasNextLine()){ // Scanner durchlaufen
                        String read = sc.nextLine(); // Einzelne Zeile speichern
                        // Create Befehl
                        if(read.startsWith("create")){ 
                                mis = false; // kein Input Mismatch da zu allererst ein Create Befehl erfolgt
                                read = read.substring(7); // Create String kürzen
                                Scanner sc2 = new Scanner(read); // Scanner für den Create String
                                int a = sc2.nextInt(); // Erste Zahl nach dem Create
                                int b = sc2.nextInt(); // zweite Zahl nach dem Create
                                // Falls eine der Beiden eingaben 0 oder weniger ist Input Mismatch
                                if( a <= 0){ 
									mis = true;}
							    if( b <= 0){
									mis = true;}
                                // AsciiImage Methode aufrufen
                                bild.AsciiImage(a,b);
                        }
                        // load Befehl
                        else if(read.startsWith("load")){
                                abbr = read.substring(5); // load String kürzen
                                String image = ""; // Hilfsstring fürs einlesen
                                int anz = bild.getHeight() * bild.getLength(); // Berechnung der gesamten Bildpunkte
                                while(sc.hasNextLine() && read.equals(abbr)!= true){ // Schleife durchlaufen solange es neue Lines gibt und die Abbruchbedingung nicht erfüllt ist
                                        
                                        read = sc.nextLine(); // eine Zeile einlesen
                                        image += read; // und an den Hilfsstring übergeben
                                        if( image.length() > (anz + abbr.length())){ // Sollte die Anzahl der Pixel im Hilfsstring größer sein als die Anzahl der Pixel in
											mis = true;// der Berechnung + die länge der Abbruchbedingung. Input Mismatch
											}
                                }       
                                bild.load(image); // load Methode 
                        }
                        // print Befehl zur Ausgabe des Bildes
                        else if(read.equals("print")){
                                        System.out.print(bild.toString());
                        }
                        // clear Befehl
                        else if(read.equals("clear")){
                                        bild.clear();
                                        System.out.println("");
                        }
                        // Transpose Befehl
                        else if(read.equals("transpose")){
                                        bild.transpose();
                        }
                        // Replace Befehl
                        else if(read.startsWith("replace")){
                                bild.replace(read.charAt(8),read.charAt(10));
                        }
                        // draw Line Befehl
                        else if(read.startsWith("line")){
                                read = read.substring(5); // String kürzen
                                Scanner sc3 = new Scanner(read); // Scanner für die Werte im line Befehl
                                
                                int x0 = sc3.nextInt();
                                int y0 =sc3.nextInt();
                                int x1 =sc3.nextInt();
                                int y1 =sc3.nextInt();
                                char c = read.charAt(read.length()-1);
                                
                                bild.drawLine(x0,y0,x1,y1,c);
                              
                        }
                       
                        else {
							//falls etwas anderes in Read steht Unknown Command
							System.out.println("UNKNOWN COMMAND");
                        
                        
                        }
                }
                // Ausgabe des Input Mismatch
                 if(mis == true){
							System.out.println("INPUT MISMATCH");}

        }
}
