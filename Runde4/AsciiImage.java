public class AsciiImage{

	private int height; 
	private int length;
	private char[][] bild; // gesamtes Bild in mehrdimensionalem Array

// Bilderzeugung

	public void AsciiImage(int length, int height){
	
		this.height = height;
		this.length = length;
		bild = new char[height][length];
		for(int i = 0; i < height; i++){ // Schleife f�r Zeilen
			for(int c = 0; c < length; c++){ //Schleife f�r Zeichen in einer Zeile
				bild[i][c]= '.'; // Bild mit Punkten f�llen
			}


		}
		
	}
	
// Bild�bergabe
	public void load(String image){ 
	
		for(int i = 0; i < height; i++){ // Schleife f�r Zeilen
			for(int c = 0; c < length; c++){ // Schleife f�r Zeichen in der Zeile
				bild[i][c] = image.charAt(((length)*i)+c); // Char aus dem Strin nehmen und richtig einsetzen
			}
		}
	}
// Bild l�schen �hnlich dem Code in der Methode AsciiImage
	public void clear(){
	
		for(int i = 0; i < height; i++){
			for(int c = 0; c < length; c++){
				bild[i][c]= '.';
			}


		}
		
	}
	
// Getter H�he	
	public int getHeight(){
		return height;
		}
		
// Getter L�nge
	public int getLength(){
		return length;
	}
	
// Replace Funktion


	public void replace(char oldChar, char newChar){
		for(int i = 0; i < height; i++){ // Array durchlaufen und sofern ein oldChar gefunden wir diesen durch den neuen ersetzen.
			for(int c = 0; c < length; c++){
				if (bild[i][c] == oldChar){
				bild[i][c]= newChar;
				}
			}


		}
		
	}
	
// Draw Line Methode. Die 4 F�lle sind bis auf einige Variablen identisch. Je nach Input wird die richtige ausgew�hlt.
	public void drawLine(int x0, int y0, int x1, int y1, char c){
		double dx = x1-x0; // Berechnung Delta X
		double dy = y1 - y0; // Berechnung Delta Y
		double ansteigend; // ansteigend auf X oder Y Achse
		double delta; // Dx durch Dy
		
// Fall 1
		if(Math.abs(dy)<=Math.abs(dx) && dx>=0){ // Aufsteigend dan der X Achse
			delta=dy/dx;
			ansteigend = y0;
			for (int a=x0;a<=x1;a++){
				setPixel(y0,a,c); // SetPixel aufrufen und den entsprechenden Pixel zeichnen
				ansteigend+=delta; // Ansteigend erh�hen
				y0=(int)Math.round(ansteigend); // Ansteigend Runden und auf y0 casten.
				}
			
			}
		
// Fall 2		
		else if(Math.abs(dy)<=Math.abs(dx) && dx<0){ 
			ansteigend = y0;
			delta = dy/dx;
			for (int a=x0;a>=x1;a--){
				setPixel(y0,a,c);
				ansteigend-=delta;
				y0 = (int)Math.round(ansteigend);
				}

		}
		
// Fall 3
		else if (Math.abs(dy)>Math.abs(dx) && dy>=0){
			ansteigend = x0;
			delta = dx/dy;
				for ( int a =y0; a<=y1;a++){
				setPixel(a,x0,c);
				ansteigend +=delta;
				x0 =(int)Math.round(ansteigend);
				}
			}
	
// Fall 4
		else if (Math.abs(dy)>Math.abs(dx) && dy<0){
			ansteigend = x0;
			delta = dx/dy;
			for ( int a =y0; a>=y1;a--){
				setPixel(a,x0,c);
				ansteigend -=delta;
				x0 =(int)Math.round(ansteigend);
				}

			}
			
	
		
 	} 
 	
 // Setter Methode f�r Pixel
	public void setPixel(int x, int y, char c){
		bild[x][y] = c;
	}
	
// getter Methode f�r Pixel
	public char getPixel(int x, int y){
		
		return bild[y][x];
	}
	
// Transpose funktion		
	public void transpose(){
		char[][] bild1= new char[length][height]; //Neues mehrdimensionales Array erzeugen dessen H�he und L�nge vertauscht sind.
		for (int e = 0; e<height; e++) { 
			for(int a = 0; a<length; a++) {
				
				//Bildpunkte umschreiben
				bild1[a][e] = bild[e][a];
			}
		}
		//Bild Ueberschreiben
		bild = bild1; 
		//Hoehe und Breite umschreiben
		int a = length;
		length = height;
		height = a;
	}
	
// Ausgabe Methode
	public String toString(){
		String test =""; //Ausgabestring
		for(int i = 0; i < height; i++){ //Array Durchlaufen
			for(int c = 0; c < length; c++){
				test += Character.toString(bild[i][c]); // Chars richtig an den String �bergeben
			}
			test += "\n"; // nach jeder Zeile einen Zeilenumbruch einf�gen.
		}
		return test;
		
	}

}
