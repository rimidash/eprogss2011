import java.util.Scanner;

public class AsciiShop{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		String read = sc.nextLine();
		int counter = 0;
		AsciiImage bild = new AsciiImage();
		boolean trans = false;
		
		if(read.startsWith("read")){
			counter = Integer.valueOf(read.substring(5));
			
			while(sc.hasNextLine()){
			
				read = sc.nextLine();
				
				if (read.equals("uniqueChars")){
					System.out.println(bild.uniqueChars());
				}
				else if(read.startsWith("flip-v")){
					bild.flipv();	
					
				}
				else if(read.equals("transpose")){
					bild.transpose();
					trans = true;
				}
				else if(read.startsWith("symmetric-h")){
					System.out.println(bild.symmetric());	
				}
				else {
					
					bild.addLine(read);
				}
				
			}
			
		}
		
		
		if (bild.addLine() && (bild.getHeight() == counter | (trans && bild.getLength() == counter))){
				
			System.out.print(bild.toString());
			System.out.println(bild.getLength() + " " + bild.getHeight());
	
		}
		else{
			System.out.println("INPUT MISMATCH");
		}
	}

}
