import java.util.*;

public class AsciiShop{
	public static void main (String [] args) {
		Scanner sc = new Scanner(System.in); //Scanner
		AsciiImage bild = new AsciiImage(); //Neues AsciiBilderstellen
		String read = sc.next(); //String
		int counter;
		boolean uniqu = false; //abfrage f�r befehl uniqueChars
		boolean sym = false; //abfrage f�r Symmetrie befehl
		if(read.startsWith("read")) { //read befehl einlesen
			counter = sc.nextInt();
			while (sc.hasNextLine()) {
				read = sc.nextLine();
				if (read.startsWith("flip-v")) { //flip befehl einlesen
					bild.flipV(); //flippen
				}
				else if (read.startsWith("uniqueChars")){ //uniqueChars befehl einlesen
					uniqu = true; //abfrage true setzen
				}
				else if (read.startsWith("transpose")){ //Transpose befehl
					bild.transpose(); //transposen
				}
				else if (read.startsWith("symmetric-h")){ //Symmetrie befehl einlesen
					sym = true; //abfrage true
				}
				else {
					bild.addLine(read); //Zeile einlesen
				}
			}
			if (bild.addLine() == true && counter == bild.getHeigth()) { //wenn Zeile Stimmt
				if (uniqu == true) { //Uniquechars �berpr�fen?
					System.out.println(bild.getUniqueChars()); //Uniquechars ausgeben
				}
				else if (sym == true) { //Symmetrie �berpr�fen
					System.out.println(bild.isSymmetricH()); //symmetrie ausgeben
				}
					System.out.print(bild.toString());

				System.out.println(bild.getWidth() + " " + bild.getHeigth()); //H�he breite ausgeben
			}
			else { // bei fehler in addLine
				System.out.println("INPUT MISMATCH");
			}
		}
	}
}
