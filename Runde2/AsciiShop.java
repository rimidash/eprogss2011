import java.util.Scanner;

public class AsciiShop {
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		String Row = sc.next(); //String f�r Scanner
		int c = 0; // Speicher f�r Read Befehl
		while( sc.hasNextLine()){
			if ( Row.equals("read")) {
				if ( sc.hasNextInt()) {
				c = sc.nextInt(); //readbefehl speichern
				int x; //Laenge
				int y; //Breite
				boolean mis = true; //mismatch
				boolean key = true; //Key-richtigkeit
				boolean deco = false; //decode vorhanden
				String a = ""; // Zwischenspeicher f�r Ascii Bild
				String dec =""; // Speicher f�r Decode Befehl
				x = 0; //Variablen ausnullen
				y = -1; //Zeilencounter -readzeile
				//ueberpfruefen ob Zeile vorhanden
				while(sc.hasNextLine()){
					//Zeile in Stringvariable speicher
					Row = sc.nextLine();
					
					if (Row.startsWith("decode")) {
						dec = Row.substring(7); //decode befehl ausfiltern nur Key speichern.
						int j = 0; //counter
						String key1 = "";//String f�r key�berpr�fung.
						while (j < dec.length()) {
							//Counter zu Integer
							key1 = Integer.toString(j);
							//erstes vorkommen des Strings
							int ind = dec.indexOf(key1);
							//letztes vorkommen des Strings
							int lind = dec.lastIndexOf(key1);
							if (ind != lind) {
								
								key = false;
								break;
							}
							//string kommt nicht vor
							else if (ind == -1){
								key = false;
								break;
								
							}
							else {
								j++;
								deco = true;
							}
							
						}
						break;
						
					}
					else {
						a = a + Row; // Zeile speichern
						//Zeilenlaenge festsetzen
						if ( x == 0){
							x = Row.length();	
						}
						//Zeilenlaenge ueberpruefen
						else if (x != Row.length()) {
							//bei Fehler Mismatch-Ausgabe
							mis = false;
							//System.out.println("INPUT MISMATCH");
							break;
						}
						//Zeilencounter erhoehen
						y++;
					}
						
				}
				//Decode-Befehl �berpr�fen
				if (deco == true) {
					int o = a.length() % dec.length();
					if(o!=0) {
			
						key = false;
					}
					//entschl�sseln
					else {
						String out2 = "";//Zwischenspeicher f�r Umformung
						int count = 0; //Z�hler
						String ent2 = ""; //Zwischenspeicher f�r Entschl�sselung blockweise
						while ( count < a.length()) {
							ent2 = a.substring(count, count+dec.length()); //Blockl�nge festlegen.
							int count2 = 0; //counter Zeichenweise
							while (count2 < ent2.length()){
								//Character Umwandlung zu String
								String ent = Character.toString(dec.charAt(count2));
								// Stringwert festlegen
								int ent1 = Integer.valueOf(ent).intValue();
								//Entschl�sseln
								out2 = out2 + ent2.charAt(ent1);
								//Zeichencountererh�hen
								count2++;
							}
							//Blockcountererh�hen
							count = count + dec.length();
						}
						//Entschl�sselung an Ascii-Speicher �bergeben
						a = out2;
				
					
					}
					
				}
				
				//wenn keine Fehler vorhanden
				if (mis == true && c == y && key == true) {
					//Zeichen- und Zeilenanzahl ausgeben
					boolean leng = false;
					while (leng == false) {
						//Zeile ausgeben
						System.out.println(a.substring(0,x));
						// String um Zeile k�rzen
						a = a.substring(x);
						//�berpr�fen ob a nicht leer ist.
						leng = a.isEmpty();
					}
					// H�he & Breite ausgeben
					System.out.println(x + " " + y);
				}
				//Key-Fehlerausgabe
				else if (key == false) {
					System.out.println("INVALID KEY");
				}
				//Bildfehler Ausgabe
				else {
					System.out.println("INPUT MISMATCH");
				}
			}
		}
		//Kein Read-Befehl vorhanden.
		else {
			System.out.println("INPUT MISMATCH");
			break;
		}
		
	}
	
}
}
