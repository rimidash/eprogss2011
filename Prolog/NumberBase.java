import java.util.Scanner;

/**
* Dieses Beispiel setzt voraus, das Sie den Umgang mit Zahlen in
* verschiedenen Zahlensystemen beherrschen.
* Liest zunächst eine Ganzzahl n ein. Dann werden Zahlenbasen a und b
* eingelesen. Obwohl n eigentlich eine Zahl im Dezimalsystem ist,
* wird der Wert n als Ziffernfolge einer Zahl in der 
* Darstellung zur Basis a interpretiert
* und in eine Zahlendarstellung zur Basis b umgewandelt.
* Beispiel: n=101 a=2 b=10, Ausgabe: 5, da 101 als Binärzahl (a=2) 
* interpretiert im Dezimalsystem (b=10) 5 ergibt.
* Die Umrechnung passiert in 2 Schritten (siehe unten).
*/
public class NumberBase {
	public static void main (String [] args) {
		//Scanner wird zum Einlesen der 3 Zahlen benötigt
		Scanner sc = new Scanner (System.in); 
		
		//Die 3 Zahlen werden der Reihe nach eingelesen
		int n = sc.nextInt();
		int a = sc.nextInt();
		int b = sc.nextInt();
		
		
		/* Schritt 1:
		 * Obwohl n eigentlich eine Zahl im Dezimalsystem ist, wird
		 * sie nun als eine Ziffernfolge einer Zahl in einem anderen
		 * Zahlensystem interpretiert (Zahlenbasis a). Der Wert dieser
		 * Zahl wird als Zahl im Dezimalsystem in number10 gespeichert.
		 */
		
		/*
		 * number10 ist eine Variable zum Speichern des
		 * Zwischenergebnisses: Die eingegebene Zahl wird hier
		 * in Ihrer Darstellung als Zahl zur Basis 10 gespeichert.
		 */
		int number10 = 0;
		 
		/* 
		 * v speichert die Wertigkeit der aktuellen Ziffer.
		 * z.B. v = 100, wenn gerade die Hunderterstelle
		 * bestimmt wird. Wir beginnen mit der Einerstelle, d.h.
		 * v = 1.
		 */
		int v = 1;
		
		while (n>0) {
			/* 
			 * mit % und / werden die einzelnen Ziffern
			 * von n der Reihe nach ausgelesen,
			 * mit Ihrer Wertigkeit multipliziert und aufaddiert.
			 */
			int d = (n%10);
			number10 += v*d;
			
			/*
			 * Jeden einzelnen Schritt ausgeben. Die folgende 
			 * Anweisung kann auch entfernt werden:
			 */
			System.out.printf("n=%d\td=%d\tv=%d\tnumber10=%d%n",n,d,v,number10);
			
			n/=10;
			v*=a; //Die Zahlenbasis sei a
		}
		
		System.out.println("number10="+number10);
		
		/* Schritt 2: Umwandlung vom Zehnersystem in das b-Zahlensystem.
		 * Hier verwenden wir einfach den Algorithmus aus 
		 * "OutputDigits.java". 
		 * Wir müssen nur das Literal "10" durch den Wert der
		 * Basis b ersetzen.
		 * Dadurch erhalten wir die einzelnen 
		 * Ziffern der Zahl im b-Zahlensystem in der richtigen 
		 * Reihenfolge.
		 */
		 
		
		System.out.print("number"+b+"=");
		
		/* 
		 * v speichert wieder die Wertigkeit der aktuellen Ziffer.
		 * Wir beginnen mit der Einerstelle, d.h. v = 1.
		 */
		v = 1;
		
		/* Analog zu OutputDigits.java:
		 * In der folgenden Schleife wird der Wert der höchsten
		 * Ziffer der Zahl in der b-Darstellung gefunden. 
		 * Nach Abbruch der Schleife ist v allerdings einmal zu oft
		 * mit b multipliziert worden, daher wird danach einmal 
		 * dividiert.
		 */
		while (number10/v > 0) {
			v *= b;
		}
		
		v /= b;
	
		/* Algorithmus aus OutputDigits.java, Beschreibung siehe dort */
		while (v > 0) {
			int d = number10/v;
			System.out.print(d);
			
			number10%=v;
			v /= b;
			
		}
		
		System.out.println(); //Ein Zeilenvorschub zum Abschluss
	}
}
