import java.util.Scanner;

/**
 * Liest eine positive Ganzzahl ein und gibt deren Ziffernsumme
 * (in der Dezimaldarstellung) am Bildschirm aus.
 */
public class DigitSum {
	public static void main (String [] args) {
		//Scanner wird zum Einlesen der Zahl n benötigt
		Scanner sc = new Scanner (System.in);
		
		//Speichert die Summe der Ziffern
		int sum = 0;
		
		//n wird eingelesen
		int n = sc.nextInt();
		
		/* beginnend von der Ziffer mit der geringsten Wertigkeit
		 * (Einerstelle) werden die einzelnen Ziffern aufaddiert.
		 * Dabei wird die Einerstelle zur Ziffernsumme addiert und
		 * danach verworfen (ganzzahlige Division durch 10). 
		 * Dadurch rückt die Zehnerstelle an die
		 * Einerstelle nach. Der Vorgang wird wiederholt, bis es
		 * keine weitere Stelle mehr gibt.
		 */
		while (n > 0) {
			//d speichert Einerstelle
			int d = n%10;
			
			//d wird zu sum dazuaddiert
			sum += d;
			
			/*
			 * Jeden einzelnen Schritt ausgeben. Die folgende 
			 * Anweisung kann auch entfernt werden:
			 */
			System.out.printf("n=%d\td=%d\tsum=%d%n",n,d,sum);
			
			//Einerstelle wird verworfen. Zehnerstelle rückt nach.
			n /= 10;
		}
		
		System.out.println(sum);
	}
}
