import java.util.Scanner;

/**
* Übung des Umgangs mit Kontrollstrukturen und mit Scanner.
* Liest eine Folge von Ganzzahlen ein und erzeugt eine Ausgabe,
* mit der gefilterten Zahlenfolge: Negative Werte werden nicht 
* ausgegeben, Folgen von 0 werden als eine einzige 0 ausgegeben.
* -1 -1 -1 ist die Folge, die das Ende der Eingabe markiert.
*/
public class FilterSamples {
	public static void main (String [] args) {
		Scanner sc = new Scanner (System.in);
		
		//zählt mit, wieviele -1 hintereinander aufgetreten sind.
		int stoppers = 0;
		
		//war die letzte Zahl > 0
		boolean nonZero = true;
		
		// Wiederholen, solange nicht -1 -1 -1 aufgetreten ist
		while (stoppers < 3) {
			// Ist die folgende Zeichenkette ein Numeral?
			if (sc.hasNextInt()) {
				
				//laut Angabe
				int input = sc.nextInt(); 
				if (input > 0) {
					System.out.print(input+" ");
					stoppers = 0;
					nonZero = true;
				} else if (input == 0) {
					stoppers = 0;
					if (nonZero) { 
						System.out.print("0 ");
					}
					nonZero = false;
				} else {
					if (input == -1) {
						stoppers++;
					} else {
						stoppers = 0;
					}
				}
				
			} else {
				/* nicht numerisch interpretierbare 
				 * Zeichenkette konsumieren.
				 */
				String dummy = sc.next();
			}
		}
		
		//Abschießender Zeilenvorschub
		System.out.println();
	}
}
