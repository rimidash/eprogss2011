import java.util.Scanner;

/**
 * Liest eine positive Ganzzahl n ein (Vorbedingung!) und
 * gibt den Funktionswert n! (Fakultät) am Bilschirm aus.
 *
 * Es gilt: n! = n * (n-1) * (n-2) * ... * 1
 */
public class Fac {
	public static void main (String [] args) {
		Scanner sc = new Scanner (System.in); 
		
		int n = sc.nextInt();
		
		int fac = 1;
		
		for (int j = 1; j<=n; j++) {
			fac*=j;
		}
		
		System.out.println(fac);
	}
}
