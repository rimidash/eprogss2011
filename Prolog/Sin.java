import java.util.Scanner;

/**
* Approximiert den Sinus mit Hilfe der Taylorreihe:            
*                                                              
*  sin(x)=summe(i=0 ... n){ (-1)^i * x^(2*i+1) / (2*i+1)! }    
*                                                              
* ^ bedeutet Potenz ("hoch"), ! bedeutet Fakultät	       
*                                                              
* Beispiel: n=2: sin(x) =                                      
*                (-1)^0 * x^(2*0+1) / (2*0+1)!                 
*              + (-1)^1 * x^(2*1+1) / (2*1+1)!                 
*              + (-1)^2 * x^(2*2+1) / (2*2+1)!                 
*
* Liest zuerst ein Argument x für die Sinusfunktion sin(x) 
* und danach den Grad der Approximation n ein.
* Danach wird das Ergebnis (approximierte Sinusfunktion) 
* am Bildschirm ausgegeben. 
*/ 

public class Sin {
	public static void main (String [] args) {
		
		//Scanner wird zum Einlesen benötigt
		Scanner sc = new Scanner (System.in); 
		
		// x und n wird eingelesen.
		double x = sc.nextDouble();
		int n = sc.nextInt();
		
		//zum Speichern des Ergebnisses
		double result = 0;
		
		for (int i = 0; i<=n; i++) {
			/* 
			* fac: zum Speichern von (2*i+1)!
			* "long" ist wie "int" ein Datentyp zum
			* Speichern ganzer Zahlen. "long" hat den Vorteil
			* eines größeren Wertebereichs (jede long-Variable
			* braucht aber auch doppelt soviel Speicherplatz).
			* Wir verwenden "long" statt "int", da die 
			* Fakultätsfunktion k! schnell mit k wächst und der
			* Wertebereich von "int" auch bei kleinem n
			* überschritten wird.
			*/
			long fac = 1;
			
			//Berechnung (2*i+1)!. Siehe Fac.java
			for (int j = 1; j<=2*i+1; j++) {
				fac*=j;
			}
			
			//Addieren des (n+1)ten Terms gemäß obiger Formel
			result += Math.pow(-1,i) * Math.pow(x,2*i+1) / fac;
		}
		
		System.out.println(result);
	}
}
