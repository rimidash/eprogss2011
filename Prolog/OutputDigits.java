import java.util.Scanner;

/**
* Eingelesen wird eine Ganzzahl. Danach werden die einzelnen
* Ziffern dieser Zahl in der richtigen Reihenfolge (absteigende Wertigkeit,
* also z.B. Hunderterstelle vor Zehnerstelle vor Einerstelle), getrennt 
* durch ein Leerzeichen, ausgegeben.
*/
public class OutputDigits {
	public static void main (String [] args) {
		//Scanner wird zum Einlesen von n benötigt
		Scanner sc = new Scanner (System.in); 
		
		//Die Zahl wird eingelesen und in n gespeichert
		int n = sc.nextInt();
		
		/* 
		 * v speichert die Wertigkeit der aktuellen Ziffer.
		 * z.B. v = 100, wenn gerade die Hunderterstelle
		 * bestimmt wird. Wir beginnen mit der Einerstelle, d.h.
		 * v = 1.
		 */
		int v = 1;
		
		/* In der folgenden Schleife wird der Wert der höchsten
		 * Ziffer der Zahl in der Dezimaldarstellung gefunden. 
		 * Nach Abbruch der Schleife ist v allerdings einmal zu oft
		 * mit 10 multipliziert worden, daher wird danach einmal 
		 * dividiert. v ist dann eine Zehnerpotenz mit genausovielen
		 * Stellen wie n.
		 */
		while (n/v > 0) {
			v *= 10;
		}
		
		v /= 10;
	
		/* In der folgenden Schleife werden der Reihe nach die 
		 * Ziffern der Zahl ausgegeben. Wir beginnen mit der 
		 * höchsten Stellen (v wurde ja oben berechnet).
		 */
		while (v > 0) {
			
			//Die aktuelle Stelle wird in d gespeichert.
			int d = n/v;
			
			//Ausgabe von d
			System.out.print(d);
			System.out.print(" ");
			
			/* Höchste Stelle der Zahl wegwerfen = den Rest
			 * der Division durch v betrachten.
			 */
			n%=v;
			
			/* n wurde nun um eine Stelle kürzer. Das muss auch
			 * bei der Wertigkeit der höchsten Stelle
			 * berücksichtigt werden...
			 */
			v /= 10;
		}
		
		//Abschießender Zeilenvorschub
		System.out.println();
	}
}
