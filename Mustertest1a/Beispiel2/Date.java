

/**
 * Diese Klasse repr&auml;sentiert ein Datum. Intern werden dabei
 * Tag, Jahr und Monat als Integerwerte gespeichert.
 */
public class Date
{
    //FILL IN
    
    /**
     * Konstruktor, initialisiert die Klasse mit den &uuml;bergebenen Werten.
     * @param d Tag
     * @param m Monat
     * @param y Jahr
     */
    public Date(int d,int m, int y)
    {
        //FILL IN
    }
    
    /**
     * Konstruktor, initialisiert die Klasse mit den &uuml;bergebenen Werten.
     * @param d Tag
     * @param m Monatsnamen
     * @param y Jahr
     */
    public Date(int d,String m,int y)
    {
        //FILL IN
    }
    
    /**
     * &Uuml;berpr&uuml;ft die G&uuml;ltigkeit des Datums.
     * Dabei ist zu beachten, dass die Datumskomponenten in g&uuml;ltigen 
     * bereichen liegen (Jahr>=0, Monat: 1-12, Tag: 1-30,1-31,1-28 je nach Monat).
     * Schaltjahre sind nicht zu beachten.
     * @return True wenn g&uuml;ltig, sonst false.
     */   
    public boolean check()
    {
        //FILL IN
        return false;
    }
    
    
    /**
     * Liefert das Datum als String. z.B. "9.11.2007", bei ung&uuml;ltigem 
     * Datum soll "invalid date" zur&uuml;ckgeliefert werden.
     * @return Textdarstellung des Datums.
     */
    public String toString()
    { 
        //FILL IN
        return null;
    }
}

