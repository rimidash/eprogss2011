

/**
* Diese Klasse repr&auml;sentiert ein Datum. Intern werden dabei
* Tag, Jahr und Monat als Integerwerte gespeichert.
*/
public class Date
{
	private int d;
	private int m;
	private int y;
	
	/**
	* Konstruktor, initialisiert die Klasse mit den &uuml;bergebenen Werten.
	* @param d Tag
	* @param m Monat
	* @param y Jahr
	*/
	public Date(int d,int m, int y)
	{
		this.d = d;
		this.m = m;
		this.y = y;
	}
	
	/**
	* Konstruktor, initialisiert die Klasse mit den &uuml;bergebenen Werten.
	* @param d Tag
	* @param m Monatsnamen
	* @param y Jahr
	*/
	public Date(int d,String m,int y)
	{
		String months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec";
		
		java.util.Scanner sc = new java.util.Scanner(months);
		
		for (int i = 1; i<=12; i++) {
			if (sc.next().equals(m)) {
				this.m = i;
			}
		}
		
		this.d = d;
		this.y = y;
	}
	
	/**
	* &Uuml;berpr&uuml;ft die G&uuml;ltigkeit des Datums.
	* Dabei ist zu beachten, dass die Datumskomponenten in g&uuml;ltigen 
	* bereichen liegen (Jahr>=0, Monat: 1-12, Tag: 1-30,1-31,1-28 je nach Monat).
	* Schaltjahre sind nicht zu beachten.
	* @return True wenn g&uuml;ltig, sonst false.
	*/   
	public boolean check()
	{
		if (y >= 0 && m>0 && m<=12 && d > 0) {
			if (m==4|| m==6 ||m==9||m==11) {
				if (d <= 30) return true;
			} else if (m==2) {
				if (d <= 28) return true;
			} else {
				if (d <= 31) return true;
			}
		}
		
		return false;
	}
	
	
	/**
	* Liefert das Datum als String. z.B. "9.11.2007", bei ung&uuml;ltigem 
	* Datum soll "invalid date" zur&uuml;ckgeliefert werden.
	* @return Textdarstellung des Datums.
	*/
	public String toString()
	{ 
		if (check()) {
			return d+"."+m+"."+y;
		}
		
		return "invalid date";
	}
}

