import java.util.Scanner;

/**
* Diese Klasse verwendet einen <code>java.util.Scanner</code>, um die 
* Datumskomponenten einzulesen und daraus ein Datumsobjekt zu erstellen.
*/
public class DateReader
{
	private Scanner sc;
	
	/**
	* Konstruktor.
	* @param input Scanner zum Einlesen des Datums.
	*/
	public DateReader(Scanner input)
	{
		sc = input;
	}
	
	/**
	* Liest die Datumskomponenten (Tag Monat Jahr) vom Scanner und liefert
	* ein Datumsobjekt zur&uuml;ck. Monat kann dabei eine Zahl oder der Monatsname 
	* sein. Wenn ein Fehler passiert wird null zur&uuml;ckgeliefert.
	* (Achtung: readNextDate muss im Fehlerfall nicht immer 3 Lexikale 
	* konsumieren). 
	* (Ung&uml;ltige Date Objekte werden nicht zur&uuml;ckgeliefert).
	* @return Datumsobjekt. Wenn ein Fehler aufgetreten ist, null.
	*/
	public Date readNextDate()
	{
		int d = 0;
		int m = 0;
		int y = 0;
		String mStr = null;
		
		if (sc.hasNextInt()) {
			d = sc.nextInt();
		} 
		/* else Zweig kann auch weggelassen werden:
		 * da im Fehlerfall d == 0 bleibt, ist das Datum 
		 * ungültig und check() (siehe unten) liefert false.
		 */
		else return null;
		
		if (sc.hasNextInt()) {
			m = sc.nextInt();
		} else if (sc.hasNext()) {
			mStr = sc.next();
		} //else Zweig weggelassen
		
		if (sc.hasNextInt()) {
			y = sc.nextInt();
		} //else Zweig weggelassen 
		
		Date date;
		
		if (mStr != null) {
			date = new Date(d,mStr,y);
		} else {
			date = new Date(d,m,y);
		}
		
		if (date.check()) {
			return date;
		} else {
			return null;
		}
	}
}

