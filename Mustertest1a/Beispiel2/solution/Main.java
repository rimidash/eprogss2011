/**  
 * Hauptprogramm, testet die Funktionen der Klassen Date und DateReader
 * <br>
 * <b>Achtung: Diese Datei k&ouml;nnen Sie zum Testen Ihres Programmes 
 * benutzen, ihr Inhalt wird nicht beurteilt.</b>
 * <br>
 */ 
public class Main
{

    public static void main (String [] args)
    {
        java.util.Scanner sc=new java.util.Scanner(System.in);
        DateReader dr=new DateReader(sc);
        
        
        //<<1 1 2007
        System.out.println(dr.readNextDate());
        //>>1.1.2007
        
        //<<1 13 2007
        System.out.println(dr.readNextDate());
        //>>null

        //<<1 Jan 2007
        System.out.println(dr.readNextDate());
        //>>1.1.2007

        //<<30 2 2007
        System.out.println(dr.readNextDate());
        //>>null                
    }
}

