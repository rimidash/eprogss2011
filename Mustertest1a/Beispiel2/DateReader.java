import java.util.Scanner;

/**
 * Diese Klasse verwendet einen <code>java.util.Scanner</code> um die 
 * Datumskomponenten einzulesen und daraus ein Datumsobjekt zu erstellen.
 */
public class DateReader
{
    //FILL IN

    /**
     * Konstruktor.
     * @param input Scanner zum Einlesen des Datums.
     */
    public DateReader(Scanner input)
    {
        //FILL IN
    }
    
    /**
     * Liest die Datumskomponenten (Tag Monat Jahr) vom Scanner und liefert
     * ein Datumsobjekt zur&uuml;ck. Monat kann dabei eine Zahl oder der Monatsname 
     * sein. Wenn ein Fehler passiert wird null zur&uuml;ckgeliefert.
     * (Achtung: readNextDate muss im Fehlerfall nicht immer 3 Lexikale 
     * konsumieren). 
     * (Ung&uml;ltige Date Objekte werden nicht zur&uuml;ckgeliefert).
     * @return Datumsobjekt. Wenn ein Fehler aufgetreten ist, null.
     */
    public Date readNextDate()
    {
        //FILL IN
        return null;
    }
}

