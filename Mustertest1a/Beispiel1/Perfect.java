
/**
 * Gibt alle perfekte Zahlen in einem bestimmten Bereich aus.
 * Eine  perfekte Zahl ist eine Ganzzahl die so gross ist, wie die Summe ihrer
 * echten Teiler ( ohne Rest). Zum Beispiel ist 6 eine perfekte Zahl, da die Summe
 * der echten Teiler (1 + 2 + 3) wiederum 6 ergibt. 6 ist die kleinste perfekte Zahl.<br>
 * In Java steht zur Berechnung des Rests der Modulus-Operator % zur
 * Verf&uuml;gung. 
 */
 public class Perfect 
{
	/**
	 * Gibt alle perfekten Zahlen im Bereich von from bis inklusive to aus. 
	 * Sie k&ouml;nnen diese Methode ausf&uuml;hren in dem Sie das 
	 * Hauptprogramm "Main" ausf&uuml;hren. Die Ausgaben sollen in dieser 
	 * Methode mit System.out.println erzeugt werden.
	 * @param from Start der Suche
	 * @param to Ende der Suche
	 */
	public static void printPerfect(int from, int to)
	{
		//FILL IN
	}
}
