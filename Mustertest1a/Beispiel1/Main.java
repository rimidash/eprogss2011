
/**  
 * Hauptprogramm, testet die Funktionen der Klasse Perfect
 * <br>
 * Achtung: Diese Datei k&ouml;nnen Sie zum Testen Ihres 
 * Programmes benutzen, ihr Inhalt wird nicht beurteilt.</b>
 * <br>
 */ 
public class Main
{
	public static void main (String [] args)
	{
		Perfect.printPerfect(1,28);
		//6
		//28

		Perfect.printPerfect(30,10000);
		//496
		//8128
		
	}
}
